<?php
namespace PHP2USE\APIs\Foursquare;

use PHP2USE\Site;
use PHP2USE\APIs\SocialAPI;
use PHP2USE\APIs\SocialResource;
use PHP2USE\Common as Common;

class Bridge extends SocialAPI {
    public function html5_header () {
        // <meta property="og:locale" content="fr_fr" />
?>
<?php
    }
    public function html5_footer () {
?>
        
<?php
    }
    
    /***************************************************************************************************/
    
    public function hybrid_name () { return 'Foursquare'; }
    public function hybrid_strategy () {
        return array (
            "enabled"         => true,
            "keys"            => array(
                "id"     => $this->creds['api_key'],
                "secret" => $this->creds['api_pki'],
            ),
            'scope'           => implode(', ', array(
                'email', 'user_about_me', 'user_birthday', 'user_hometown',
            )),
            'display' => 'popup', // optional
        );
    }
    
    /***************************************************************************************************/
    
    public function initialize () {
        //Site::load_foss('facebook4php', 'src/facebook');
    }
    public function connect ($creds, $cfg, $vault) {
        
    }
    
    /***************************************************************************************************/
    
    protected function call ($verb, $uri, $args=array()) {
        $args['client_id']     = $this->creds['api_key'];
        $args['client_secret'] = $this->creds['api_secret'];
        
        $args['v']             = '20140806';
        $args['m']             = 'foursquare';
        
        $url  = "https://api.foursquare.com/{$uri}?".http_build_query($args);
        
        $resp = $this->call_json($verb, $url);
        
        switch (intval($resp->meta->code)) {
            case 200:
                return $resp;
                break;
            case 400:
                die("Bad request");
                break;
            case 401:
                die("Unauthorized");
                break;
            case 403:
                die("Forbidden");
                break;
            case 404:
                die("Not found");
                break;
            case 405:
                die("Method not allowed");
                break;
            case 409:
                die("Conflict");
                break;
            case 410:
                die("Param Error");
                break;
            case 500:
                die("Internal Server error");
                break;
            default:
                print_r($resp);
                die(1);
                break;
        }
        
        return $resp;
    }
    
    public function venue($uid=null) {
        if ($uid==null) {
            $uid = $this->cfg['venue_id'];
        }
        
        $resp = $this->invoke('GET', "v2/venues/{$uid}");
        
        return $this->reflect('FoursquareVenue', $resp, function ($obj) {
            return $obj->response->venue;
        });
    }
}

class FoursquareVenue extends SocialResource {
    public static function narrow($obj) {
        return $obj->id;
    }
    
    protected function initialize() {
        
    }
    
    public function uid ()        { return $this->res->id; }
    public function link ()       { return $this->res->canonicalUrl; }
    
    public function likes ()      { return $this->res->likes->count; }
    public function checkins ()   { return $this->res->stats->checkinsCount; }
    public function users ()      { return $this->res->stats->usersCount; }
    public function tips ()       { return $this->res->stats->tipCount; }
    public function hereNow ()    { return $this->res->hereNow; }
    
    public function categories () { return $this->res->categories; }
    public function contact ()    { return $this->res->contact; }
    public function location ()   { return $this->res->location; }
    public function openHours ()  { return $this->res->hours; }
}

