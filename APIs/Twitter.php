<?php
namespace PHP2USE\APIs\Twitter;

use PHP2USE\Site;
use PHP2USE\APIs\SocialAPI;
use PHP2USE\APIs\SocialResource;
use PHP2USE\Common as Common;

class Bridge extends SocialAPI {
    public function html5_header () {
?>
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@GaliPhotographe"/>
    <meta name="twitter:domain" content="m-gali.com"/>
    <meta name="twitter:creator" content="@GaliPhotographe"/>
<?php
    }
    public function html5_footer () {
?>
        
<?php
    }
    
    /***************************************************************************************************/
    
    public function hybrid_name () { return 'Twitter'; }
    public function hybrid_strategy () {
        return array (
            "enabled"         => true,
            "keys"            => array(
                "id" => $this->creds['api_key'],
                "secret" => $this->creds['api_pki'],
            ),
        ),
    }
    
    /***************************************************************************************************/
    
    public function initialize ($creds, $cfg, $vault) {
        Site::load_foss('twitter4php', 'src/twitter.class');
    }
    public function connect ($creds, $cfg, $vault) {
        
    }
    
    /***************************************************************************************************/
    
    protected function call ($uri, $args=array()) {
        $url  = "https://graph.facebook.com/{$uri}?access_token={$this->vault['access_token']}&{$args}";
        
        $resp = $this->call_json('GET', $url);
        
        return $resp;
    }
}

