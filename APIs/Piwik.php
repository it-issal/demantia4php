<?php
namespace PHP2USE\APIs\Piwik;

use PHP2USE\Site;
use PHP2USE\APIs\SocialAPI;
use PHP2USE\APIs\SocialResource;
use PHP2USE\Common as Common;

class Bridge extends SocialAPI {
    public function html5_header () {
?>
        
<?php
    }
    public function html5_footer () {
        $cfg = Site::get('stats.piwik');
        
        if (is_array($cfg)) {
            $link = $cfg[0];
            $s_id = $cfg[1];
?>
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://<?php echo $link ?>/" : "http://<?php echo $link ?>/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", <?php echo $s_id ?>);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://<?php echo $link ?>/piwik.php?idsite=<?php echo $s_id ?>" style="border:0" alt="" /></p></noscript>
<?php
        }
    }
    
    /***************************************************************************************************/
    
    public function initialize ($creds, $cfg, $vault) {
        
    }
    public function connect ($creds, $cfg, $vault) {
        
    }
}

